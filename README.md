# Renovate config

Renovate (Bot) is a CLI tool that regularly scans your Git repositories for outdated dependencies. If it finds any, it automatically creates a pull request (PR) in which the dependency is updated (for GitLab, the terminology is Merge Request). Ref: https://www.augmentedmind.de/2021/07/11/renovate-bot-introduction/

Github: https://github.com/renovatebot/renovate

## Repository content

- **default.json**
  - A global configuration explicitly referenced in the `renovate.json` file.
  - `automerge` is enabled for a few rules:
    - "patch", "pin", "digest"
    - "@types/"
- **renovate.json**
  - A configuration file used by Renovate while scanning a repository and making merge requests if dependencies are out of date. Each repository has own `renovate.json` file.

## Renovate user/bot

Make sure the user that owns the RENOVATE_TOKEN PAT is a member of the corresponding GitLab projects/groups with the right permissions. Ref: https://docs.renovatebot.com/modules/platform/gitlab/

## default.json

If you have enough confidence in your Merge Requests configuration, you can allow `automerge`:

```
{
  "matchUpdateTypes": ["minor", "patch", "pin", "digest"],
  "automerge": false  -> true
}
```

## GitLab Merge Requests configuration (recommended)

**Merge method:** Fast-forward merge

**Merge options:** Enable "Delete source branch" option by default

**Squash commits when merging:** Require

**Merge commit message template**

```text
%{title} (%{reference})

%{issues}

Merge branch '%{source_branch}' into '%{target_branch}'.
```

**Squash commit message template**

```text
%{title} (%{reference})
```
